# Ejemplo de CI

Este ejemplo demuestra como automatizar los tests de un proyecto PHP que
verifica ante cualquier commit, los tests pasen considerando:

* PHP versiones 5.6 y 7
* Integración con Postgres y MySQL

El ejemplo se extrajo de los [ejemplos propuestos por Gitlab](https://docs.gitlab.com/ce/ci/examples/php.html)

